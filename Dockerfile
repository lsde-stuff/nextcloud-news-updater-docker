FROM python:alpine
RUN wget https://github.com/nextcloud/news-updater/archive/master.zip -O - | unzip -d /tmp -
WORKDIR /tmp/news-updater-master
RUN python3 setup.py install --install-scripts=/usr/bin && rm -rf /tmp/news-updater-master
USER nobody
CMD nextcloud-news-updater -c /run/secrets/news-updater/config.ini
